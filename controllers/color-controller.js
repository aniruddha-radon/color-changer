app.controller("colorController", function($scope) {
    $scope.backgroundStyle = { 'background-color': 'white' };

    $scope.changeColor = function() {
        const colorCharacters = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];
        let backgroundColor = '';
        for(let i = 0; i < 6; i++) {
            let index = Math.floor(Math.random() * 16);
            backgroundColor += colorCharacters[index];
        }

        $scope.backgroundStyle['background-color'] = '#' + backgroundColor;
    }
})